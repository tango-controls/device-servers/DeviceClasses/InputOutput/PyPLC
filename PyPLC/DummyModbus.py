
from fandango.tango import *
import tango
from tango.server import Device, device_property, attribute, command, pipe

def int2bin(n, length=16):
    bstr = bin(n)
    bstr = bstr.replace("0b", "")
    if len(bstr) < length:
        bstr = "0" * (length - len(bstr)) + bstr
    return "0b" + bstr[-length:] 

class DummyModbus(Device):
    
    def init_device(self,*args,**kwargs):
        super().init_device(*args,**kwargs)
        self.raw_data = defaultdict(int)
        self.Load()
        
    @command
    def Load(self):
        db = tango.Database()
        data = db.get_device_property(self.get_name(),'RawData')
        for r in data['RawData']:
            r = r.split(':')
            self.raw_data[int(r[0])] = int(r[1])
            
    @command
    def Save(self):
        db = tango.Database()
        data = ['{}:{}'.format(k,v) for k,v in sorted(self.raw_data.items())]
        db.put_device_property(self.get_name(),{'RawData':data})
        
    @command(dtype_in=int,
             dtype_out=str)
    def Int2Bin(self,argin):
        return int2bin(argin)
    
    @command(dtype_in=tango.CmdArgType.DevVarLongArray,
             dtype_out=tango.CmdArgType.DevVarLongArray)
    def ReadHoldingRegisters(self,argin):
        addr = range(argin[0],argin[0]+argin[1])
        argout = [self.raw_data[i] for i in addr]
        return argout
    
    @command(dtype_in=int,
             dtype_out=tango.CmdArgType.DevVarStringArray)
    def ReadBinaryRegister(self,argin):
        argout = self.raw_data[argin]
        return [str(argout),int2bin(argout)]

    @command(dtype_in=tango.CmdArgType.DevVarLongArray,
             dtype_out=bool)    
    def PresetSingleRegister(self,argin):
        print('PresetSingleRegister',argin)
        addr = argin[0]
        self.raw_data[addr] = argin[1]
        return True      

    @command(dtype_in=tango.CmdArgType.DevVarLongArray,
             dtype_out=int)
    def Flag(self,argin):
        print('Flag',argin)
        addr, n = argin[0],argin[1]
        reg = int(self.ReadHoldingRegisters([addr,1])[0])
        #tval = self.Denary2Binary(reg)
        #argout = int(tval[15-n])
        bval = int2bin(reg)
        r = bval[-(1+n)]
        print(bval,reg,n,r)
        return int(r)
        
    @command(dtype_in=tango.CmdArgType.DevVarLongArray,
             dtype_out=int)
    def WriteFlag(self,argin):
        print('WriteFlag',argin)
        addr,n,flag = [int(i) for i in argin[:3]]
        reg = int(self.ReadHoldingRegisters([argin[0],1])[0])
        flag = str(int(bool(flag)))
        bval = int2bin(reg)
        _1, _2 = bval[:-(n+1)], bval[-n:] if n else ''
        print('WriteFlag',bval,n,_1,flag,_2)
        nval = _1 + flag + _2
        ival = int(nval,2)
        print('WriteFlag',bval,nval,n,flag)
        self.PresetSingleRegister([addr,ival])
        return ival
    
if __name__ == "__main__":
     DummyModbus.run_server()
