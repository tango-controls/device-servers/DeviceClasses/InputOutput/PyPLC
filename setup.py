#!/usr/bin/env python
# Always prefer setuptools over distutils
from setuptools import setup, find_packages

__doc__ = """

To install as system package:

  python setup.py install
  
To install as local package, just run:

  mkdir /tmp/builds/
  python setup.py install --root=/tmp/builds
  /tmp/builds/usr/bin/$DS -? -v4

To tune some options:

  RU=/opt/control
  python setup.py egg_info --egg-base=tmp install --root=$RU/files --no-compile \
    --install-lib=lib/python/site-packages --install-scripts=bin

-------------------------------------------------------------------------------
"""
DS='PyPLC'
description = 'Tango device for Modbus equipment'
version = str(open(DS+'/VERSION').read().strip())
license = 'GPL-3.0'

## Launcher scripts
scripts = [
  #DS,
  #'./bin/'+DS,
  ]

## This option relays on DS.py having a main() method
entry_points = {
        'console_scripts': [
            '%s = %s.%s:main'%(DS,DS,DS),
        ],
}


package_dir = {DS: DS,}
packages = package_dir.keys()
package_data = {DS: ['VERSION'],}

setup(name = DS.lower(),
      author = 'Sergi Rubio',
      author_email="srubio@cells.es",
      version = version,
      license = license,
      description = description,
      packages = packages,
      package_dir= package_dir,
      scripts = scripts,
      entry_points = entry_points,
      include_package_data = True,
      package_data = package_data,
      install_requires=['fandango'], #,'PyTango'],
     )
